DVlop - Just another web application development framework
=======

DVlop is a framework to develop web applications. The ideia is to integrate other colaborative jobs like CakePHP, cake plugins and templates to make a strong base to any project.

Some things used
----------------

[CakePHP](http://www.cakephp.org) - The rapid development PHP framework

[DebugKit](https://github.com/cakephp/debug_kit) - Official CakePHP Debug Kit Git Repository http://cakephp.org

[Charisma](http://usman.it/free-responsive-admin-template) - free, responsive, multiple skin admin template

[CakePHP-Media-Plugin](https://github.com/bmcclure/CakePHP-Media-Plugin) - A CakePHP (2.0) plugin enabling transfer/manipulation/embedding of files in 23 ways.

[Search Plugin](https://github.com/CakeDC/search) - Search Plugin for CakePHP http://cakedc.com

[Tags](https://github.com/CakeDC/tags) - Tags Plugin for CakePHP http://cakedc.com

[Users](https://github.com/CakeDC/users) - Users Plugin for CakePHP http://cakedc.com

[Utils](https://github.com/CakeDC/utils) - Utils Plugin for CakePHP http://cakedc.com

[domPDF](https://github.com/fdeschenes/cakephp-dompdf-view) - A CakePHP plugin for creating and/or rendering PDF documents using dompdf

[Boletos](https://github.com/Danielpk/boletos) - Plugin para gerar boletos com o CakePHP

## Contributing to this Plugin ##

Please feel free to contribute to the plugin with new issues, requests, unit tests and code fixes or new features. If you want to contribute some code, create a feature branch from develop, and send us your pull request. Unit tests for new features and issues detected are mandatory to keep quality high. 

## License ##

Licensed under [The MIT License](http://www.opensource.org/licenses/mit-license.php)
Redistributions of files must retain the above copyright notice.

## Copyright ###

GeBender 2014<br/>
[Fazejamento Iteracional Aplicado]<br/>
http://gebender.com.br<br/>